var gulp = require('gulp');
var uglifycss = require('gulp-uglifycss');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;



gulp.task('hello' , function() {
	console.log('Hello world!')
});

gulp.task('sass', function(){
    return gulp.src('sass/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./css'))

})

gulp.task('ugly', () => {
    return gulp.src('css/*.css')
      .pipe(uglifycss({
        "maxLineLen": 10,
        "uglyComments": true
      }))
      .pipe(gulp.dest('ugly/'));
  });

gulp.task('mini', () => {
    return gulp.src('css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('mini'));
});

gulp.task('concat', function() {
  return gulp.src(['sass/style.scss','sass/style2.scss'])
  .pipe(concat('style.scss'))
  .pipe(gulp.dest('scss'))
});

gulp.task('concatcustom', function() {
  return gulp.src(['sass/style.scss','sass/style2.scss'])
  .pipe(concat('style.scss'))
  .pipe(gulp.dest('scssconcat'))
  .pipe (gulp.src('scssconcat/syle.scss')
  .pipe(sass())
  .pipe(gulp.dest('./css')));
});

gulp.task('autoreload', function () {
  gulp.src('sass/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./css'))
  browserSync.init({
      server: {
          baseDir: "./"
      }
  });
  ['sass']
  gulp.watch("index.html").on("change", reload);
  gulp.watch('sass/style.scss', gulp.series('sass')).on("change", reload);
});
