


$(".imagem-esquerda").hover(function(){
  if($('.direita')[0].className != 'direita cheio' && $('.esquerda')[0].className != 'esquerda cheio'){
    $(".esquerda").css("z-index","2");
    $(".direita").css("z-index","4");
  }
});
$(".imagem-direita").hover(function(){
  if($('.direita')[0].className != 'direita cheio' && $('.esquerda')[0].className != 'esquerda cheio'){
    $(".esquerda").css("z-index","4");
    $(".direita").css("z-index","2");
  }
});

window.onresize = function(){
  if(innerWidth < 790 && $('.direita')[0].className != 'direita cheio' && $('.esquerda')[0].className != 'esquerda cheio'){
    $(".figura").attr("src","imagens/projetos/logo-sm-branco.png");
    $(".figura").addClass("change-branco");
  }
  else if($('.direita')[0].className != 'direita cheio' && $('.esquerda')[0].className != 'esquerda cheio'){
    $(".figura").removeClass("change-branco");
    $(".figura").removeClass("change-preto");
    $(".figura").attr("src","imagens/tela-inicial/logo-branco.png");
  }
}


if(innerWidth < 790){
  $(".figura").attr("src","imagens/projetos/logo-sm-branco.png");
  $(".figura").addClass("change-branco");
}

$(document).on('scroll', function() {

  //imagens de fundo alternando
    var SA = $(this).scrollTop()
    var HA = window.innerHeight

    if(SA >= innerHeight){
      $('.testetela').css("display", "none")
    }
    else{
      $('.testetela').css("display", "unset")
    }

    if($('.direita')[0].className == 'direita cheio'){
      cor = '255, 255, 255'
    }
    else{
      cor = '43, 43, 43'
    }

    if(SA <= HA/18){
      $('.testetela').css("background-color", "rgba("+cor+")")
    }
    else if(SA <= (HA/18)*2){
      $('.testetela').css("background-color", "rgba("+cor+", 0.900)")
    }
    else if(SA <= (HA/18)*3){
      $('.testetela').css("background-color", "rgba("+cor+", 0.850)")
    }
    else if(SA <= (HA/18)*4){
      $('.testetela').css("background-color", "rgba("+cor+", 0.800)")
    }
    else if(SA <= (HA/18)*5){
      $('.testetela').css("background-color", "rgba("+cor+", 0.750)")
    }
    else if(SA <= (HA/18)*6){
      $('.testetela').css("background-color", "rgba("+cor+", 0.700)")
    }
    else if(SA <= (HA/18)*7){
      $('.testetela').css("background-color", "rgba("+cor+", 0.650)")
    }
    else if(SA <= (HA/18)*8){
      $('.testetela').css("background-color", "rgba("+cor+", 0.600)")
    }
    else if(SA <= (HA/18)*9){
      $('.testetela').css("background-color", "rgba("+cor+", 0.550)")
    }
    else if(SA <= (HA/18)*10){
      $('.testetela').css("background-color", "rgba("+cor+", 0.500)")
    }
    else if(SA <= (HA/18)*11){
      $('.testetela').css("background-color", "rgba("+cor+", 0.450)")
    }
    else if(SA <= (HA/18)*12){
      $('.testetela').css("background-color", "rgba("+cor+", 0.400)")
    }
    else if(SA <= (HA/18)*13){
      $('.testetela').css("background-color", "rgba("+cor+", 0.350)")
    }
    else if(SA <= (HA/18)*14){
      $('.testetela').css("background-color", "rgba("+cor+", 0.300)")
    }
    else if(SA <= (HA/18)*15){
      $('.testetela').css("background-color", "rgba("+cor+", 0.250)")
    }
    else if(SA <= (HA/18)*16){
      $('.testetela').css("background-color", "rgba("+cor+", 0.200)")
    }
    else if(SA <= (HA/18)*17){
      $('.testetela').css("background-color", "rgba("+cor+", 0.150)")
    }
    else{
      $('.testetela').css("display", "rgba("+cor+", 0.100)")
    }
  });




  

$('#n_foi').click(function(){
  toastr.warning('Verifique se todos os campos foram preenchidos')
})
$('#doc_excluido').click(function(){
  toastr.success('Documento excluido!')
})
$('#n_foi_doc').click(function(){
  toastr.warning('Verifique se todos os campos estão preenchidos e tente novamente.')
})
$('#foi_doc').click(function(){
  toastr.success('Recarregue a página', 'Documento adicionado!')
})
// window.onresize = function(event) {
//   console.log(innerWidth)
//   if(innerWidth < 550){
//     $('.addimg').html("<img class='figura' src='imagens/projetos/logo-sm-branco.png'></img>")
//   }
//   else{
//     $('.addimg').html("<img class='figura' src='imagens/tela-inicial/logo-branco.png'></img>")
//   }
// };

// console.log(innerWidth)
// if(innerWidth < 550){
//   $('.addimg').html("<img class='figura' src='imagens/projetos/logo-sm-branco.png'></img>")
// }




$(document).ready(function(){
  $(".figura").click(function(){
    $(".pai").removeClass("change");
    $(".esquerda").removeClass("vazio");
    $(".esquerda").removeClass("cheio");
    $(".esquerda").addClass("hover-esquerda");
    $(".imagem-esquerda").css("cursor","pointer");
    $(".imagem-esquerda").removeClass("cheio");
    $("#container-tela2").css("display","none");
    $(".p-projetos").removeClass("cheio");
    $(".direita").removeClass("cheio");
    $(".direita").removeClass("vazio");
    $(".direita").addClass("hover-direita");
    $(".imagem-direita").css("cursor","pointer");
    $(".imagem-direita").removeClass("cheio");
    $(".imagem-direita").removeClass("vazio");
    $("#grupo-esquerda").removeClass("cheio");
    $("#grupo-direita").removeClass("cheio");
    $("#grupo-direita.cheio").css("margin-top","-50vh");
    $("#container-tela3").css("display","none");
    $(".p-documentos").removeClass("cheio");
    $(".p-documentos").removeClass("vazio");
    $("#flexis").removeClass("vazio");
    $("#container-tela4").removeClass("cheio");
    $(".hamburguer span").addClass("change-preto");
    $(".hamburguer span").removeClass("change-branco");
    setTimeout(function(){
      $(".hamburguer span").addClass("change-preto");
      $(".hamburguer span").removeClass("change-branco");
    }, 600);
    

    if(innerWidth < 790){
      $(".figura").attr("src","imagens/projetos/logo-sm-branco.png");
      $(".figura").addClass("change-branco");
    }
    else{
      $(".figura").attr("src","imagens/tela-inicial/logo-branco.png");
      $(".figura").removeClass("change-preto");
      $(".figura").removeClass("change-branco");
    }

  })



/*Menu Projetos*/
  $(".overlay-menu li:first-child a").click(function(){
    /*Se estiver do lado direito , ai ja sai*/;
    $(".esquerda").removeClass("vazio");
    $(".esquerda").css("z-index","4");
    $(".direita").css("z-index","2");
    $(".direita").removeClass("cheio");
    $(".imagem-direita").removeClass("cheio");
    $("#container-tela3").css("display","none");
    $(".p-documentos").removeClass("cheio");
    $(".p-documentos").addClass("vazio");
    $(".hamburguer span").removeClass("change-branco");
    $(".hamburguer span").removeClass("change-preto");
    $(".figura").removeClass("change-preto");
    $(".figura").removeClass("change-branco");
    $("#grupo-direita").removeClass("cheio");
    $("#flexis").removeClass("vazio");
    $("#container-tela4").removeClass("cheio");


      /*Ir para os projetos*/
      $(".pai").addClass("change");
      $(".esquerda").addClass("cheio");
      $(".direita").addClass("vazio");
      $(".imagem-esquerda").addClass("cheio");
      $(".imagem-direita").addClass("vazio");
      $(".esquerda").removeClass("hover-esquerda");
      $(".imagem-esquerda").css("cursor","default");
      $("#container-tela2").css("display","flex");
      $(".p-projetos").addClass("cheio");
      $(".figura").attr("src","imagens/projetos/logo-sm-branco.png");
      $(".figura").addClass("change-branco");
      $(".hamburguer span").removeClass("change-preto");
      $(".hamburguer span").addClass("change-branco");
      $("#grupo-esquerda").addClass("cheio");
});

$(document).on("scroll", function() {
  

  if($('.direita')[0].className == 'direita cheio'){

    corCima = 'preto'
    corBaixo = 'branco'
  }
  else{
    corCima = 'branco'
    corBaixo = 'preto'
  }

  var SA = $(this).scrollTop()
  var HA = window.innerHeight

  if (SA >= HA) {

    $(".hamburguer span").removeClass("change-"+corCima);
    $(".hamburguer span").addClass("change-"+corBaixo);
    $(".figura").attr("src","imagens/projetos/logo-sm-"+corBaixo+".png");
    $(".figura").removeClass("change-"+corCima);
    $(".figura").addClass("change-"+corBaixo);

  }
  else{
    $(".hamburguer span").removeClass("change-"+corBaixo);
    $(".hamburguer span").addClass("change-"+corCima);
    $(".figura").attr("src","imagens/projetos/logo-sm-"+corCima+".png");
    $(".figura").removeClass("change-"+corBaixo);
    $(".figura").addClass("change-"+corCima);
  }
});

/*Menu documentos*/
      $(".overlay-menu li:nth-child(2) a").click(function(){

        /*Se estiver do lado esquerdo , ai ja sai*/
        $(".esquerda").removeClass("cheio");
        $(".esquerda").css("z-index","2");
        $(".direita").css("z-index","4");
        $(".direita").removeClass("vazio");
        $(".p-documentos").removeClass("cheio");
        $(".p-documentos").addClass("vazio");
        $(".imagem-esquerda").removeClass("cheio");
        $(".imagem-esquerda").addClass("vazio");
        $(".esquerda").addClass("hover-esquerda");
        $(".imagem-esquerda").css("cursor","pointer");
        $("#container-tela2").css("display","none");
        $(".p-projetos").removeClass("cheio");
        $(".hamburguer span").removeClass("change-branco");
        $(".hamburguer span").removeClass("change-preto");
        $("#grupo-esquerda").removeClass("cheio");
        $("#flexis").removeClass("vazio");
        $("#container-tela4").removeClass("cheio");


        /*ir para a direita*/

        $(".pai").addClass("change");
        $(".esquerda").addClass("vazio");
        $(".direita").addClass("cheio");
        $(".direita").removeClass("hover-direita");
        $(".imagem-direita").addClass("cheio");
        $("#container-tela3").css("display","flex");
        $(".p-documentos").addClass("cheio");
        $(".p-documentos").removeClass("vazio");
        $(".figura").attr("src","imagens/documentos/logo-sm-preto.png");
        $(".figura").removeClass("change-branco");
        $(".figura").addClass("change-preto");
        $(".hamburguer span").removeClass("change-branco");
        $(".hamburguer span").addClass("change-preto");
        $("#grupo-direita").addClass("cheio");
        $("#grupo-direita").removeClass("margem");

        /*Scroll*/
    });


    /*Menu insercao*/
    $(".overlay-menu li:nth-child(3) a").click(function(){

      /*Se estiver do lado esquerdo , ai ja sai*/
      $(".esquerda").removeClass("cheio");
      $(".esquerda").css("z-index","2");
      $(".direita").css("z-index","4");
      $(".direita").removeClass("vazio");
      $(".p-documentos").removeClass("cheio");
      $(".p-documentos").addClass("vazio");
      $(".imagem-esquerda").removeClass("cheio");
      $(".imagem-esquerda").addClass("vazio");
      $(".esquerda").addClass("hover-esquerda");
      $(".imagem-esquerda").css("cursor","pointer");
      $("#container-tela2").css("display","none");
      $(".p-projetos").removeClass("cheio");
      $(".hamburguer span").removeClass("change-branco");
      $(".hamburguer span").removeClass("change-preto");
      $("#grupo-esquerda").removeClass("cheio");

    /*Se estiver do lado direito , ai ja sai*/;
      $(".esquerda").removeClass("vazio");
      $(".esquerda").css("z-index","4");
      $(".direita").css("z-index","2");
      $(".direita").removeClass("cheio");
      $(".imagem-direita").removeClass("cheio");
      $("#container-tela3").css("display","none");
      $(".p-documentos").removeClass("cheio");
      $(".p-documentos").addClass("vazio");
      $(".hamburguer span").removeClass("change-branco");
      $(".hamburguer span").removeClass("change-preto");
      $(".figura").removeClass("change-preto");
      $(".figura").removeClass("change-branco");
      $("#grupo-direita").removeClass("cheio");

      /*ir para a insercao*/

      $(".pai").removeClass("change");
      $("#container-tela4").addClass("cheio");
      $("#flexis").addClass("vazio");
      $(".figura").removeClass("change-branco");
      $(".figura").removeClass("change-preto");
      $(".hamburguer span").removeClass("change-preto");
      $(".hamburguer span").removeClass("change-branco");
      if(innerWidth < 790){
        $(".figura").attr("src","imagens/projetos/logo-sm-branco.png");
        $(".figura").addClass("change-branco");
      }
      else{
        $(".figura").attr("src","imagens/tela-inicial/logo-branco.png");
        $(".figura").removeClass("change-preto");
        $(".figura").removeClass("change-branco");
      }



  });
  });




    /*Lado Esquerdo*/
  
    $(".imagem-esquerda").click(function esquerda(){

       $(".pai").addClass("change");
       $(".esquerda").addClass("cheio");
       $(".direita").addClass("vazio");
       $(".esquerda").css("z-index","4");
       $(".direita").css("z-index","2");
       $(".p-documentos").removeClass("cheio");
       $(".p-documentos").addClass("vazio");
       $(".imagem-esquerda").addClass("cheio");
       $(".imagem-direita").addClass("vazio");
       $(".esquerda").removeClass("hover-esquerda");
       $(".imagem-esquerda").css("cursor","default");
       $("#container-tela2").css("display","flex");
       $(".p-projetos").addClass("cheio");
       $(".figura").attr("src","imagens/projetos/logo-sm-branco.png");
       $(".figura").removeClass("change-preto");
       $(".figura").addClass("change-branco");
       $(".hamburguer span").removeClass("change-preto");
       $(".hamburguer span").addClass("change-branco");
       $("#grupo-esquerda").addClass("cheio");
       $("#grupo-direita").removeClass("cheio");
       $("#grupo-direita").removeClass("margem");

});
//p-projetos

  $(".p-projetos").click(function(){
    $(".pai").addClass("change");
    $(".esquerda").addClass("cheio");
    $(".direita").addClass("vazio");
    $(".esquerda").css("z-index","4");
    $(".direita").css("z-index","2");
    $(".p-documentos").removeClass("cheio");
    $(".p-documentos").addClass("vazio");
    $(".imagem-esquerda").addClass("cheio");
    $(".imagem-direita").addClass("vazio");
    $(".esquerda").removeClass("hover-esquerda");
    $(".imagem-esquerda").css("cursor","default");
    $("#container-tela2").css("display","flex");
    $(".p-projetos").addClass("cheio");
    $(".figura").attr("src","imagens/projetos/logo-sm-branco.png");
    $(".figura").removeClass("change-preto");
    $(".figura").addClass("change-branco");
    $(".hamburguer span").removeClass("change-preto");
    $(".hamburguer span").addClass("change-branco");
    $("#grupo-esquerda").addClass("cheio");
    $("#grupo-direita").removeClass("cheio");
    $("#grupo-direita").removeClass("margem");
  });

//Lado Direito
  $(document).ready(function direita(){
      $(".imagem-direita").click(function(){
      
        $(".pai").addClass("change");
        $(".esquerda").addClass("vazio");
        $(".esquerda").css("z-index","2");
        $(".direita").css("z-index","4");
        $(".direita").addClass("cheio");
        $(".direita").removeClass("hover-direita");
        $(".imagem-direita").addClass("cheio");
        $(".imagem-direita").css("cursor","default");
        $("#container-tela3").css("display","flex");
        $(".p-documentos").addClass("cheio");
        $(".figura").attr("src","imagens/documentos/logo-sm-preto.png");
        $(".figura").removeClass("change-branco");
        $(".figura").addClass("change-preto");
        $(".hamburguer span").removeClass("change-branco");
        $(".hamburguer span").addClass("change-preto");
        $("#grupo-esquerda").removeClass("cheio");
        $("#grupo-direita").addClass("cheio");
        $("#grupo-direita").removeClass("margem");

});
//p-documentos
  $(".p-documentos").click(function(){
        $(".pai").addClass("change");
        $(".esquerda").addClass("vazio");
        $(".esquerda").css("z-index","2");
        $(".direita").css("z-index","4");
        $(".direita").addClass("cheio");
        $(".direita").removeClass("hover-direita");
        $(".imagem-direita").addClass("cheio");
        $(".imagem-direita").css("cursor","default");
        $("#container-tela3").css("display","flex");
        $(".p-documentos").addClass("cheio");
        $(".figura").attr("src","imagens/documentos/logo-sm-preto.png");
        $(".figura").removeClass("change-branco");
        $(".figura").addClass("change-preto");
        $(".hamburguer span").removeClass("change-branco");
        $(".hamburguer span").addClass("change-preto");
        $("#grupo-esquerda").removeClass("cheio");
        $("#grupo-direita").addClass("cheio");
        $("#grupo-direita").removeClass("margem");
  });

  $(".troca-inv").click(function opa(){
    $(".troca-inv").toggleClass("esquerd");
    $(".troca-pro").toggleClass("es");
    $(".troca-pro").toggleClass("di");
    $(".t-pro").toggleClass("escolhido");
    $(".t-pro").toggleClass("n_escolhido");
    $(".t-doc").toggleClass("escolhido");
    $(".t-doc").toggleClass("n_escolhido");
    $("#form-insercao").toggleClass("show");
    $("#form-documentos").toggleClass("show");
  });


  });
//upload
  function ekUpload(){
    function Init() {
  
      var fileSelect    = document.getElementById('file-upload'),
          fileDrag      = document.getElementById('file-drag'),
          submitButton  = document.getElementById('submit');
  
      fileSelect.addEventListener('change', fileSelectHandler, false);
  
      var xhr = new XMLHttpRequest();
      if (xhr.upload) {
  
        fileDrag.addEventListener('dragover', fileDragHover, false);
        fileDrag.addEventListener('dragleave', fileDragHover, false);
        fileDrag.addEventListener('drop', fileSelectHandler, false);
      }
    }
  
    function fileDragHover(e) {
      var fileDrag = document.getElementById('file-drag');
  
      e.stopPropagation();
      e.preventDefault();
  
      fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
    }
  
    function fileSelectHandler(e) {
      // Fetch FileList object
      var files = e.target.files || e.dataTransfer.files;
  
      // Cancel event and hover styling
      fileDragHover(e);
  
      // Process all File objects
      for (var i = 0, f; f = files[i]; i++) {
        parseFile(f);
        uploadFile(f);
      }
    }
  
    // Output
    function output(msg) {
      // Response
      var m = document.getElementById('messages');
      m.innerHTML = msg;
    }
  
    function parseFile(file) {
      output(
        '<strong>' + encodeURI(file.name) + '</strong>'
      );
      
      // var fileType = file.type;
      // console.log(fileType);
      var imageName = file.name;
  
      var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
      if (isGood) {
        document.getElementById('start').classList.add("hidden");
        document.getElementById('response').classList.remove("hidden");
        document.getElementById('notimage').classList.add("hidden");
        // Thumbnail Preview
        document.getElementById('file-image').classList.remove("hidden");
        document.getElementById('file-image').src = URL.createObjectURL(file);
      }
      else {
        document.getElementById('file-image').classList.add("hidden");
        document.getElementById('notimage').classList.remove("hidden");
        document.getElementById('start').classList.remove("hidden");
        document.getElementById('response').classList.add("hidden");
        document.getElementById("file-upload-form").reset();
      }
    }
  
    function setProgressMaxValue(e) {
      var pBar = document.getElementById('file-progress');
  
      if (e.lengthComputable) {
        pBar.max = e.total;
      }
    }
  
    function updateFileProgress(e) {
      var pBar = document.getElementById('file-progress');
  
      if (e.lengthComputable) {
        pBar.value = e.loaded;
      }
    }
  
    function uploadFile(file) {
  
      var xhr = new XMLHttpRequest(),
        fileInput = document.getElementById('class-roster-file'),
        pBar = document.getElementById('file-progress'),
        fileSizeLimit = 1024; // In MB
      if (xhr.upload) {
        // Check if file is less than x MB
        if (file.size <= fileSizeLimit * 1024 * 1024) {
          // Progress bar
          pBar.style.display = 'inline';
          xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
          xhr.upload.addEventListener('progress', updateFileProgress, false);
  
          xhr.onreadystatechange = function(e) {
            if (xhr.readyState == 4) {
  
            }
          };
  
          // Start upload
          xhr.open('POST', document.getElementById('file-upload-form').action, true);
          xhr.setRequestHeader('X-File-Name', file.name);
          xhr.setRequestHeader('X-File-Size', file.size);
          xhr.setRequestHeader('Content-Type', 'multipart/form-data');

        } else {
          output('Porfavor escolha uma imagem menor (< ' + fileSizeLimit + ' MB).');
        }
      }
    }
  
    // Check for the various File API support.
    if (window.File && window.FileList && window.FileReader) {
      Init();
    } else {
      document.getElementById('file-drag').style.display = 'none';
    }
  }
  ekUpload();

//Menu
  $('#toggle').click(function() {
    $(this).toggleClass('active');
    $('#overlay').toggleClass('open');
    });



  $('.overlay-menu a').click(function() {
    $('#toggle').toggleClass('active');
    $('#overlay').toggleClass('open');
  });
    
  window.scroll(0, 0)
  setTimeout(function(){ window.scroll(0, 0)}, 500);