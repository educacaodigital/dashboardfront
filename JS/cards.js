
//configuração de TOASTR jquery
//toastr.success('teste descrição', 'teste title')
/*toastr.success('teste descrição', 'teste title', {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: "toast-top-right",
    preventDuplicates: false,
    onclick: null,
    showDuration: 300,
    hideDuration: 1000,
    timeOut: 5000,
    extendedTimeOut: 1000,
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut"
})*/
/////////////
oba();
function obinha(){
    $('.loadercards').css('display', 'block')
    $("#submit-line-e").remove();
    $(".manipproj").remove();
    $('#tela-projetos').remove();  
    var grupinho = `
        <div class="submit-line" id="submit-line-e">
            <div  id='forceScroll'></div>
            <input id="pesquisa-esquerda" class="txt-box" type="text" placeholder="Buscar...">
            <a href="#forceScroll"><button class="submit-lente" id="search-left" type="submit">
            <img class="lupa-branca" src="imagens/tela-inicial/lupa-branca.png">                            
            </button></a>
        </div>  
    ` 
    var gogo =`<div id="tela-projetos" class="row tela2">
        <div class='loadercards'></div>
        <div class="centro_tela2" id="centro-tela2"></div>
        <div class="navegation">
            <div class="arrow_l aprojl" id="arrowp_l"><img src="imagens/projetos/left-arrow.png" ></div>
            <div class="arrow_r aprojr" id="arrowp_r"><img src="imagens/projetos/right-arrow.png" ></div>
        </div>
    </div>`;
    $("#grupo-esquerda").append(grupinho);
    $("#container-tela2").append(gogo);
    oba();
}
function oba() {
    $(".botao-form p").addClass("hide");
    $("#enviarproj").addClass("show");
    
$.ajax({
    url: "https://api-dashboard-front.herokuapp.com/projects",
    dataType: "json",
    success: function () {

        $(".botao-form p").removeClass("hide");
        $("#enviarproj").removeClass("show"); 
        $('.loadercards').css('display', 'none')

    }
}).done (function(data) {
        
        if(innerWidth >= 1517){
            divisor=8;
        }
        else{
            divisor=6;
        }

   

    criarSlider();
    function criarSlider() {
        var quantidade_projetos = data.length/divisor;
        var id_slider = 1;
        let slidd =`
        <div class="slide" id="slide_` + id_slider + `"></div>
        `; 
        $(".tela2").append(slidd);
        montarSlider();
        function montarSlider(){
            if(quantidade_projetos>id_slider){
                id_slider++;
                var slid =`
                <div class="slide hide" id="slide_` + id_slider + `"></div>
                `; 
                $(".tela2").append(slid);
                montarSlider();
            }  
        };
    }  
    criarCards();
    function criarCards(){
        var id_slide = 1;
    for(i = 0; i < data.length; i++){
        var quant_atual = $(`#slide_${id_slide} .sli`).length;
        var count = i;
        var id = data[i]._id;
        let repositorio = data[i].repository;
        var titulo = data[i].title;
        let link = data[i].url_production; 
        let link_homo = data[i].url_homologation;
        let imagem = data[i].thumbnail;  
        let tech = data[i].tech; 
        let descricao = data[i].description;
        let descricao_edit = data [i].description;

        if(descricao_edit == null || descricao_edit == "undefined"){
            descricao_edit = "";
        }

        if(descricao == null || descricao == "undefined" || descricao == ""){
            descricao = "Não temos nada a falar sobre esse projeto..";
        }


        if(link == null || link == "undefined" || link == ""){
            link = "-";
        }

        if(link_homo == null || link_homo == "undefined" || link_homo == ""){
            link_homo = "-";
        }

        var card = `

            <div class="sli column d-flex flex-wrap"  id="card` + count + `">
                <article class="carta" id="carta` + count + `"> 
                    <div class="nrml_body" id="nrml_body` + count + `">
                        <img class="foto" src="data:image/gif;base64,` + imagem + `" >
                        <div class="informacao" id="informacao` + count + `">
                            <img class="logo-card" src="imagens/projetos/icons/` + tech + `-icon.png" >
                            <input id="key`+ count +`" class="key" style="display:none" type="text" value="`+ id +`"></input>
                            <h2 class="titulo" id="titulo`+ count +`">` + titulo + `</h2>
                            <h3 class="link" id="link`+ count +`"><a href="` + link + `" target="_blank">` + link + `</a></h3>
                            <h3 class="link-homo" id="link-homo`+ count +`"><a href="` + link_homo + `" target="_blank">` + link_homo + `</a></h3>
                            <img id="img_bit` + count + `" class="bit" src="imagens/projetos/bit.png" ><input class="bit-link" id="rep` + count + `" type="text" value="`+ repositorio +`" name="repository" disabled></input>
                            <h3 class="detalhes" id="detalhes` + count + `">
                            <img class="descricao" id="descricao` + count + `" src="imagens/projetos/visualizar.png">
                            <img id="editar` + count + `" class="editar" src="imagens/projetos/editar.png">
                            <img class="excluir" id="excluir` + count + `" src="imagens/projetos/excluir.png">
                            </h3>  
                        </div>
                    </div>

                    <div class="edit_body" id="edit_body` + count + `">
                        <img class="logo_card_edit" src="imagens/projetos/icons/` + tech + `-icon.png" >
                        <form class="edit_form" id="edit_form` + count + `" method="PUT" enctype="application/json">
                            <input class="edit-titulo" id="edit-titulo`+ count +`" type="text" value="`+ titulo +`" name="title"></input>
                            <input class="edit-link" id="edit-link`+ count +`" type="text" value="`+ link +`" name="url_production"></input>
                            <input class="edit-homo" id="edit-homo`+ count +`" type="text" value="`+ link_homo +`" name="url_homologation"></input>       
                            <img class="bit2" src="imagens/projetos/bit.png" ><input  id="edit_rep`+ count +`" class="edit-rep" type="text" value="`+ repositorio +`" name="repository"></input>
                            <textarea class="edit-desc" id="edit-desc`+ count +`" name="description" placeholder="Não temos nada a falar sobre esse projeto...">` + descricao_edit + `</textarea>
                            <h2 class=text_edit id="text_edit` + count + `">Descrição▼</h2>
                            <h2 class=text_edit2 id="text_edit2-` + count + `">Informações▲</h2>
                        </form>
                        <div class="fim-esc" id="fim-esc`+ count +`">
                            <div class="inc">
                            <img class="incorreto" id="incorreto_edit`+ count +`" src="imagens/projetos/incorreto.png">
                            </div>
                            <div class="cor">
                                <img type="submit" value="Submit" class="correto" id="correto_edit`+ count +`" src="imagens/projetos/correto.png">
                            </div>
                        </div>
                    </div>

                    <div class="del_body" id="del_body` + count + `">
                        <img class="logo_del" src="imagens/projetos/icons/` + tech + `-icon.png" >
                        <div class="tex_del" id="popo` + count + `">
                            <h2 class="titulo_del" id="titulo`+ count +`">` + titulo + `</h2>
                            <h2 id="subtitulo_del` + count + `" class="subtitulo_del">Quer mesmo excluir esse projetão?</h2>
                        </div> 
                        <div class="tex_del2" id="popopo` + count + `">
                        <h2 class="titulo_del" id="titulo`+ count +`">` + titulo + `</h2>
                        <h2 id="subtitulo_del` + count + `" class="subtitulo_del">Projetão Excluido!</h2>
                    </div>  
                        <div class="fim_esc_del" id="fim_esc_del`+ count +`">
                            <div class="inc">
                                <img class="incorreto_del" id="incorreto_del`+ count +`" src="imagens/projetos/incorreto.png">
                            </div>
                            <div class="cor">
                                <button type="button" class="bot_cor">
                                <img type="submit" value="Submit" class="correto_del" id="correto_del`+ count +`" src="imagens/projetos/correto.png"></button>
                            </div>
                        </div> 
                    </div> 
                    <div class="desc_body" id="desc_body` + count + `">
                        <h2 class="titulo_desc" id="titulo`+ count +`">` + titulo + `</h2>
                        <img class="logo_desc" src="imagens/projetos/icons/` + tech + `-icon.png">
                        <textarea class="desc" id="desc_text`+ count +`" placeholder="Não temos nada a falar sobre esse projeto..." disabled>` + descricao_edit + `</textarea>
                        <div class="fim_esc_desc" id="fim-desc`+ count +`">
                            <div class="fechar">
                                <img class="desc_fechar" id="desc_fechar`+ count +`" src="imagens/projetos/incorreto.png">
                            </div>
                        </div> 
                    </div>
                    <div id="loader` + count + `" class="loader loader--style7" title="6">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                        <rect x="0" y="0" width="4" height="20" fill="#333">
                            <animate attributeName="opacity" attributeType="XML"
                            values="1; .2; 1" 
                            begin="0s" dur="0.6s" repeatCount="indefinite" />
                        </rect>
                        <rect x="7" y="0" width="4" height="20" fill="#333">
                            <animate attributeName="opacity" attributeType="XML"
                            values="1; .2; 1" 
                            begin="0.2s" dur="0.6s" repeatCount="indefinite" />
                        </rect>
                        <rect x="14" y="0" width="4" height="20" fill="#333">
                            <animate attributeName="opacity" attributeType="XML"
                            values="1; .2; 1" 
                            begin="0.4s" dur="0.6s" repeatCount="indefinite" />
                        </rect>
                        </svg>
                    </div>
                </article>   
            </div>
    `;


//Arrumar os cards dentro dos slides


            if(quant_atual<divisor){
                $(`#slide_` + id_slide).append(card);                 
            }
            else{
                id_slide++;
                $(`#slide_` + id_slide).append(card);                
            }

        }
    };
    control2 = 0
    control = 0
    function teste456(){
        if (control == 1 || control2 == 1){
            $(".correto").click(function (event) {
        
                $(correto).addClass("hide");
                $(incorreto).addClass("hide");
                $(loader).addClass("showcard");  
        
                event.preventDefault();
                let num = event.target.id.substring(12);
                let key = $('#key' + num); 
                let chave = key.val();
                var rep = $('#edit-rep' + num)[0];
                let titulo = recebeValorInput($('#edit-titulo' + num));
                let production = recebeValorInput($('#edit-link' + num));
                let homologation = recebeValorInput($('#edit-homo' + num));
                let repositorio = recebeValorInput($('#edit_rep' + num));
                let descricao = recebeValorInput($('#edit-desc' + num));
                
                let opa = {
                    title: titulo, 
                    url_production: production,
                    url_homologation: homologation,
                    repository: repositorio,
                    description: descricao
                }
            
                let obj = JSON.stringify(opa);
                
                $(".correto").prop("disabled", true);
        
                $.ajax({
                    type: "PUT",
                    enctype: 'application/json',
                    url: "https://api-dashboard-front.herokuapp.com/projects/" + chave,
                    data: obj,
                    processData: false, 
                    contentType: 'application/json', 
                    cache: false, 
                    timeout: 600000, 
                    success: function (data) {

                        toastr.success('Projeto editado')
                        $(correto).removeClass("hide");
                        $(incorreto).removeClass("hide");
                        $(loader).removeClass("showcard");
                        $(nrml).removeClass("hide");
                        $(edit).removeClass("show");

                                    
                        document.getElementById("titulo" + num).textContent = titulo;
                        document.getElementById("link" + num).textContent = production;
                        document.getElementById("link-homo" + num).textContent = homologation;
                        document.getElementById("rep" + num).value = repositorio;
                        document.getElementById("desc_text" + num).textContent = descricao;
                    },
                    error: function (e) {
                        toastr.error('Verifique sua conexão com a internet', 'O projeto não foi modificado')
                        $(correto).removeClass("hide");
                        $(incorreto).removeClass("hide");
                        $(loader).removeClass("showcard");   
                        toastr.error('O documento não foi editado, verifique sua conexão com a rede e tente novamente', 'Erro')
                    }
                });
            });
        }
    }

    controlclick1 = 1
    window.onresize = function(){

        controlclick1 = 0

        if(innerWidth > 1600 && control !=1){
            control = 1
            teste456()
        }      
        else if(innerWidth <= 1600){
            control = 0
        }

        if(innerWidth <= 1500 && control2 !=1){
            control2 = 1
            teste456()
        }      
        else if(innerWidth > 1500){
            control2 = 0
        }

        $(".correto").click(function (event) {
            if(controlclick1 == 1){
                corretoclick(event)
            }
        });

        $(".editar").click(function(event) { 

            num = event.target.id.substring(6);
            incorreto = $('#incorreto_edit' + num);
            correto = $('#correto_edit' + num);
            loader = $('#loader' + num);
            barra = $('#fim-esc' + num);
            nrml = $('#nrml_body' + num);
            edit = $('#edit_body' + num);
            key = $('#editar' + num);
    
            $(nrml).addClass("hide");
            $(edit).addClass("show");
    
        $("#text_edit" + num).click(function(){
            $("#edit_form" + num).addClass("hide");
        });
    
        $("#text_edit2-" + num).click(function(){
            $("#edit_form" + num).removeClass("hide");
        });
    
        $(".incorreto").click(function(event) { 
            $(nrml).removeClass("hide");
            $(edit).removeClass("show");
        });
    }); 

        $(".excluir").click(function(event) { 
        
            let num = event.target.id.substring(7);
            let del = $('#del_body' + num);   
            let nrml = $('#nrml_body' + num);
            let incorreto = $('#incorreto_del' + num);
            let correto = $('#correto_del' + num);
            let loader = $('#loader' + num);
            let sub = $('#popo' + num);
            let subb = $('#popopo' + num);
            let barra = $('#fim_esc_del' + num);
    
            $(del).addClass("show");        
            $(nrml).addClass("hide");
            
            key = $('#key' + num);  
            $(correto).click(function(){
                $(correto).addClass("hide");
                $(incorreto).addClass("hide");
                $(loader).addClass("showcard");            
               
                $.ajax({  
                    url: 'https://api-dashboard-front.herokuapp.com/projects/' + key[0].value,  
                    type: 'DELETE',  
                    dataType: 'json',  
                    data:key,  
                    success: function () {  
                        var numpro = $('.manipproj')[0].innerText
                        numproj = parseInt(numpro)
                        $('.markp')[0].innerText = numproj-1
                        $('.manipproj')[0].innerText = numproj-1
                        toastr.success('Projeto excluido')
                        $(sub).addClass("hide");          
                        $(subb).addClass("show");      
                        $(barra).addClass("hide");
                        $(loader).removeClass("showcard");
      
                    },  
                    error: function () {  
                        toastr.error('Verifique sua conexão com a internet', 'Projeto não foi excluido')  
                    }  
                });
            }) 
            $(incorreto).click(function(){
                $(del).removeClass("show");
                $(nrml).removeClass("hide");
            }) 
        });  
        $(".descricao").click(function(){
            let num = event.target.id.substring(9);
            let nrml = $('#nrml_body' + num);
            let desc = $('#desc_body' + num);
        
            quantidade_sliders = $("#tela-projetos .slide").length;
        
            $(desc).addClass("show");
            $(nrml).addClass("hide");
        
            $(".desc_fechar").click(function(){
                $(desc).removeClass("show");
                $(nrml).removeClass("hide");
            })
        })
        if ($("#tela-projetos .slide").length !=  quantidade_sliders){
            for(a = 0; a <= $("#tela-projetos .slide").length; a++){
                $('.aprojl').click()
            }
            quantidade_sliders = $("#tela-projetos .slide").length;
            
            
        }
        if(innerWidth >= 1517 && divisor != 8){
            var quantidade_projetos = data.length/divisor;
            let base = 0;
            for(i = 0; quantidade_projetos >= base; i++ ){                
                base++
                let slide = document.getElementById(`slide_${base}`);
                if($("#tela-projetos .slide").length > 0){
                    slide.remove();
                }
            }
            divisor = 8;

            criarSlider();
            criarCards();
            
        }
        if(innerWidth <= 1517 && divisor != 6){
            var quantidade_projetos = data.length/divisor;           
            let base = 0;
            for(i = 0; quantidade_projetos >= base; i++ ){
                base++
                let slide = document.getElementById(`slide_${base}`);
                if($("#tela-projetos .slide").length > 0){
                    slide.remove();
                }
            }
            divisor = 6;
            
            criarSlider();

            criarCards();
                
        }
            

    };


    ////////////////////DELETE

    // $(".del_button").click(function(){
    //     location.reload();
    // });

    $("#att_button").click(function(){
        location.reload();
    });

    
    $(".excluir").click(function(event) { 
        
        var num = event.target.id.substring(7);
        var del = $('#del_body' + num);   
        var nrml = $('#nrml_body' + num);
        var incorreto = $('#incorreto_del' + num);
        var correto = $('#correto_del' + num);
        var loader = $('#loader' + num);
        var sub = $('#popo' + num);
        var subb = $('#popopo' + num);
        var barra = $('#fim_esc_del' + num);

        $(del).addClass("show");        
        $(nrml).addClass("hide");
        
        key = $('#key' + num);  
        $(correto).click(function(){
            $(correto).addClass("hide");
            $(incorreto).addClass("hide");
            $(loader).addClass("showcard");            
           
            $.ajax({  
                url: 'https://api-dashboard-front.herokuapp.com/projects/' + key[0].value,  
                type: 'DELETE',  
                dataType: 'json',  
                data:key,  
                success: function () {  
                    var numpro = $('.manipproj')[0].innerText
                    numproj = parseInt(numpro)
                    $('.markp')[0].innerText = numproj-1
                    $('.manipproj')[0].innerText = numproj-1
                    toastr.success('Projeto excluido')
                    $(sub).addClass("hide");          
                    $(subb).addClass("show");      
                    $(barra).addClass("hide");
                    $(loader).removeClass("showcard");
  
                },  
                error: function () {  
                    toastr.error('Verifique sua conexão com a internet', 'Projeto não foi excluido')  
                }  
            });
        }) 
        $(incorreto).click(function(){
            $(del).removeClass("show");
            $(nrml).removeClass("hide");
        }) 
    });    
//////////////////PUT

$(".correto").click(function (event) {
    if(controlclick1 == 1){
        corretoclick(event)
    }
});
function corretoclick(event){

    $(correto).addClass("hide");
    $(incorreto).addClass("hide");
    $(loader).addClass("showcard");  

    event.preventDefault();
    let num = event.target.id.substring(12);
    
    let key = $('#key' + num); 
    let chave = key.val();
    var rep = $('#edit-rep' + num)[0];
    let titulo = recebeValorInput($('#edit-titulo' + num));
    let production = recebeValorInput($('#edit-link' + num));
    let homologation = recebeValorInput($('#edit-homo' + num));
    let repositorio = recebeValorInput($('#edit_rep' + num));
    let descricao = recebeValorInput($('#edit-desc' + num));
    
    let opa = {
        title: titulo, 
        url_production: production,
        url_homologation: homologation,
        repository: repositorio,
        description: descricao
    }

    
    let obj = JSON.stringify(opa);
    
    $(".correto").prop("disabled", true);

    $.ajax({
        type: "PUT",
        enctype: 'application/json',
        url: "https://api-dashboard-front.herokuapp.com/projects/" + chave,
        data: obj,
        processData: false, 
        contentType: 'application/json', 
        cache: false, 
        timeout: 600000, 
        success: function (data) {

            toastr.success('Projeto editado')
            $(correto).removeClass("hide");
            $(incorreto).removeClass("hide");
            $(loader).removeClass("showcard");
            $(nrml).removeClass("hide");
            $(edit).removeClass("show");


            document.getElementById("titulo" + num).textContent = titulo;
            document.getElementById("link" + num).textContent = production;
            document.getElementById("link-homo" + num).textContent = homologation;
            document.getElementById("rep" + num).value = repositorio;
            document.getElementById("desc_text" + num).textContent = descricao;

        },
        error: function (e) {
            toastr.error('Verifique sua conexão com a internet', 'O projeto não foi modificado')
            $(correto).removeClass("hide");
            $(incorreto).removeClass("hide");
            $(loader).removeClass("showcard");   
            toastr.error('O documento não foi editado, verifique sua conexão com a rede e tente novamente', 'Erro')
        }
    });
}
    $(".editar").click(function(event) {
        num = event.target.id.substring(6);
        incorreto = $('#incorreto_edit' + num);
        correto = $('#correto_edit' + num);
        loader = $('#loader' + num);
        barra = $('#fim-esc' + num);
        nrml = $('#nrml_body' + num);
        edit = $('#edit_body' + num);
        key = $('#editar' + num); 

        $(nrml).addClass("hide");
        $(edit).addClass("show");

        $("#text_edit" + num).click(function(){
            $("#edit_form" + num).addClass("hide");
        });

        $("#text_edit2-" + num).click(function(){
            $("#edit_form" + num).removeClass("hide");
        });

        $(".incorreto").click(function(event) { 
            $(nrml).removeClass("hide");
            $(edit).removeClass("show");
        });

        
    }); 


//DESCRIPTION

$(".descricao").click(function(){
    
    
    let num = event.target.id.substring(9);
    let nrml = $('#nrml_body' + num);
    let desc = $('#desc_body' + num);

    $(desc).addClass("show");
    $(nrml).addClass("hide");

    $(".desc_fechar").click(function(){
        $(desc).removeClass("show");
        $(nrml).removeClass("hide");
    })
})

    var id_p = data.length;
    var quant_p = `
        <h1 class='manipproj'>` + id_p + `</h1>
        `;
    $("#total-p").append(quant_p)
    $('.addbadgel').prepend(`<span class="mark-r markp badge badge-secondary">${id_p}</span>`)
  
    var margem = 0;
    var zero = 0;

let atual = 1;
let proximo = 2;
let anterior = 0;
var quantidade_sliders = $("#tela-projetos .slide").length;



$("#arrowp_r").click(function(){
    slide_r()
})

verificaSetal()
haa()
function verificaSetar(){
    $('.aprojl').show()
    if(atual >= quantidade_sliders-1){
        
        $('.aprojr').hide()
    }
}
function verificaSetal(){
    $('.aprojr').show()
    if(atual <= 2){
        $('.aprojl').hide()
    }
}
function haa(){
    if(quantidade_sliders == 1){
        $('.aprojr').hide()
        $('.aprojl').hide()
    }
}


function slide_r(){
    verificaSetar()
    if(atual < quantidade_sliders){
        right();
    }
    function right() {
        $(`#slide_${atual}`).addClass("hide");
        $(`#slide_${anterior}`).addClass("hide");
        $(`#slide_${proximo}`).removeClass("hide");

        atual++
        proximo++
        anterior++
    }
}

function slide_l(){
    verificaSetal()
    if(atual > 1){
        left();    
    }   
    function left() {

        $(`#slide_${atual}`).addClass("hide");
        $(`#slide_${anterior}`).removeClass("hide");
        $(`#slide_${proximo}`).addClass("hide");

        atual--
        proximo--
        anterior--
    }
}

$("#arrowp_l").click(function(){
    slide_l()
});

//Reconhecimento de SWIPE
function swipedetect(el, callback){
    var touchsurface = el,
    swipedir,
    startX,
    startY,
    distX,
    distY,
    threshold = 150,
    restraint = 100,
    allowedTime = 300,
    elapsedTime,
    startTime,
    handleswipe = callback || function(swipedir){}
  
    touchsurface.addEventListener('touchstart', function(e){
        var touchobj = e.changedTouches[0]
        swipedir = 'none'
        dist = 0
        startX = touchobj.pageX
        startY = touchobj.pageY
        startTime = new Date().getTime()
    }, false)
  
    touchsurface.addEventListener('touchmove', function(e){
    }, false)
  
    touchsurface.addEventListener('touchend', function(e){
        var touchobj = e.changedTouches[0]
        distX = touchobj.pageX - startX
        distY = touchobj.pageY - startY
        elapsedTime = new Date().getTime() - startTime
        if (elapsedTime <= allowedTime){
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ 
                swipedir = (distX < 0)? 'left' : 'right'
            }
            else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){
                swipedir = (distY < 0)? 'up' : 'down'
            }
        }
        handleswipe(swipedir)
    }, false)
  }
  var el = document.querySelector('.tela2');
  swipedetect(el, function(swipedir){
    if(swipedir == 'right'){
      slide_l()
    }
    else if(swipedir == 'left'){
      slide_r()
    }
  });
  
//////Search bar projetos
    var lista_titulosp = [];

    for (j = 0; j < data.length; j++) {
        let titulo = data[j].title;
        lista_titulosp.push(titulo);
    }

    autocomplete(document.getElementById("pesquisa-esquerda"), lista_titulosp);

    $("#search-left").click(function(){
        let lista_titulosp2 = [];
        let valConvert = [];
        let op = $("#pesquisa-esquerda");
        let opp = $(op);
        let valor_da_pesquisa = opp[0].value;
        let link = "#card" + lista_titulosp2[0];
        let palavra = valor_da_pesquisa;
        let idx = lista_titulosp.indexOf(palavra);
        let quantidade_telas = $(".tela2 .slide").length;
        let inicial = 1;
        
        // for(i = 1; lista_titulosp2 > 6; i++){
        
        // }
        
        for(conv = 0; conv < lista_titulosp.length; conv++){
        vallor = lista_titulosp[conv]
        valConvert.push(format(vallor))
        }
        palavraConv = format(palavra)
        idx = valConvert.indexOf(palavraConv)
    
        $(".card-animation").removeClass("card-animation");
    
        for(i = 0; quantidade_telas > inicial; i++){
        document.getElementById("arrowp_l").click();
        inicial++;
        
        }
        while (idx != -1) {
        lista_titulosp2.push(idx);
        idx = lista_titulosp.indexOf(palavra, idx + 1);
        }
    
        localizaProjeto();
        function localizaProjeto(){ 
            let id_card_p = "#card" + lista_titulosp2[0];
            var tamanho_listap = lista_titulosp2[0];
            
            
            
            if(lista_titulosp2 == ""){
                toastr.warning('Tente buscar manualmente', 'Projeto não encontrado')
            }
            if(innerWidth < 1517){
                for(i = 0;tamanho_listap >= 6; i++){
                    document.getElementById("arrowp_r").click();
                    tamanho_listap = tamanho_listap - 6;
                }
            }
            else{
                for(i = 0;tamanho_listap >= 8; i++){
                    document.getElementById("arrowp_r").click();
                    tamanho_listap = tamanho_listap - 8;
                }
            }
            $(id_card_p).addClass("card-animation");
        
            setTimeout(function(){
                $(id_card_p).removeClass("card-animation");
            }, 1000);
        }
        op[0].value = ''
    })
    
    
function format(aaa){
    t1 = aaa.toUpperCase().replace(/\s/g, '')
    assentos = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜ'
  
    for(d1 = 0; d1 < assentos.length ; d1++){
      var alter = 'O'
      var local = t1.indexOf(assentos[d1])
  
      if(local != -1){
        var local2 = assentos.indexOf(t1[local])
  
        if(assentos.substring(0,6).indexOf(assentos[local2]) != -1) {
          alter = 'A'
          t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
        }
        else if(assentos[7].indexOf(assentos[local2]) != -1) {
          alter = 'C'
          t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
        }
        else if(assentos.substring(8,11).indexOf(assentos[local2]) != -1) {
          alter = 'E'
          t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
        }
        else if(assentos.substring(12,15).indexOf(assentos[local2]) != -1) {
          alter = 'I'
          t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
        }
        else if(assentos[16].indexOf(assentos[local2]) != -1) {
          alter = 'D'
          t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
        }
        else if(assentos[17].indexOf(assentos[local2]) != -1) {
          alter = 'N'
          t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
        }
        else if(assentos.substring(18,23).indexOf(assentos[local2]) != -1) {
          alter = 'O'
          t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
        }
        else if(assentos.substring(24,27).indexOf(assentos[local2]) != -1) {
          alter = 'U'
          t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
        }
      }
    }
    return t1
  
  }
  
  function autocomplete(inp, arr){

    var currentFocus;

    inp.addEventListener("input", function(e){
        var a, b, i, val = this.value;

        closeAllLists();

        if (!val){
            return false;
        };
        currentFocus = -1;

        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        
        this.parentNode.appendChild(a);
        
        for (i = 0; i < arr.length; i++) {
           if (format(arr[i]).replace(' ','').indexOf(format(val).replace(' ','')) != -1) {
              b = document.createElement("DIV");
              b.className = "search-force"
              b.innerHTML = arr[i].substr(0, val.length);
              b.innerHTML += arr[i].substring(val.length);
              b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
              b.addEventListener("click", function(e) {
                  inp.value = this.getElementsByTagName("input")[0].value;
                  if($('.direita')[0].className != 'direita cheio'){
                    $('#search-left').click()
                  }
                  else{
                    $('#search-right').click()
                  }
                  inp.value = ''
                
                  closeAllLists();
              });
              a.appendChild(b);
            }
          }
      });

      inp.addEventListener("keydown", function(e) {
          if(e.key == "Enter"){
            if($('.autocomplete-active').length > 0){
              inp.value = $('.autocomplete-active')[0].children[0].value
            }
              
            if(this.id == 'pesquisa-esquerda'){
              $('#search-left').click()
              inp.value = ''
            }
            else{
              $('#search-right').click()
              inp.value = ''
            }
          }

          var x = document.getElementById(this.id + "autocomplete-list");
          if (x) x = x.getElementsByTagName("div");
          if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
          } else if (e.keyCode == 38) { 
            currentFocus--;
            addActive(x);
          } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
              if (x) x[currentFocus].click();
            }
          }
      });
      function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
      }
      function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
          x[i].classList.remove("autocomplete-active");
        }
      }
      function closeAllLists(elmnt) {

        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
          if (elmnt != x[i] && elmnt != inp) {
            x[i].parentNode.removeChild(x[i]);
          }
        }
      }
      document.addEventListener("click", function (e) {
          closeAllLists(e.target);
      });
    }


})
.fail (function() { 
    $(".botao-form p").removeClass("hide");
    $("#enviarproj").removeClass("show"); 
})
.always (function() { 
    
});


}

$("#submit").click(function(){
    setTimeout(() => { obinha() }, 2000);

});

function recebeValorInput(campo) {
    let valor 
    valor = campo.val();

    return valor
}