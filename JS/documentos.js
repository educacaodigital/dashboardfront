

opa();
function oopa() {
    $('#tela-documentos').remove();
    $("#submit-line-d").remove();
    $(".manipdoc").remove();

    var ope = `<div class="submit-line" id="submit-line-d">
                <div  id='forceScrolll'></div>
                <input id="pesquisa-direita" class="txt-box" type="text" placeholder="Buscar...">
                <a href="#forceScrolll"><button class="submit-lente" id="search-right" type="submit">
                    <img class="botao-direita" src="imagens/tela-inicial/lupa-preta.png">
                </button></a>
            </div>` 
    var opinha = `<div id="tela-documentos" class="row tela3">
                    <div class="centro_tela" id="centro-tela3"></div>
                    <div class="arrow_l adocl" id="arrowd_l"><img src="imagens/documentos/left-arrow.png" ></div>
                    <div class="arrow_r adocr" id="arrowd_r"><img src="imagens/documentos/right-arrow.png" ></div>
                    <button type="button" id="doc_excluido"></button>
                    <div class="sliders_documentos">
                    </div>                                    
                </div>`
    $("#container-tela3").append(opinha);
    $("#grupo-direita").append(ope);  

    opa();

}
    
function opa() {
    $("#enviar p").addClass("hide");
    $("#enviardoc").addClass("show");


$.ajax({
    url: "https://api-dashboard-front.herokuapp.com/documents",
    dataType: "json",
    success: function () {
        $("#enviar p").removeClass("hide");
        $("#enviardoc").removeClass("show"); 
    }
}).done (function(data) { 

    var quantidade_documentos = data.length/12;
    if(quantidade_documentos <= 1){
        $('.adocl').hide()
        $('.adocr').hide()
    }
    var id_slider_doc = 1;
    var id_slider_doc_m = 1;
    var slido =`
        <div class="slide_doc" id="slide-doc_` + id_slider_doc + `" style="margin-left: 0vw"></div>
        `; 
    $(".sliders_documentos").append(slido);

    criarSliderDoc();
    function criarSliderDoc(){
        if(quantidade_documentos>id_slider_doc){
            id_slider_doc++;
            var slidi =`
            <div class="slide_doc" id="slide-doc_` + id_slider_doc + `" style="margin-left: `+ id_slider_doc_m +`00vw"></div>
            `; 
            $(".sliders_documentos").append(slidi);
            id_slider_doc_m++
            criarSliderDoc();
        }    
    };

    var id_slide_doc = 1;
    var x = 0;

    for(i = 0; i < data.length; i++){
        var count = i;
        let id = data[i]._id
        let titulo = data[i].title;
        let subtitulo = data[i].subtitle;
        let link = data[i].url;
        let type = data[i].type
        
    
        var doc =`
        <div class="borda item-doc doccc${count}" id="item_doc` + count +`">
            <span class="hsdocp hsdocpp${count}">
                <input id="key_doc`+ count +`" class="key" style="display:none" type="text" value="`+ id +`"></input>
                <a href="` + link + `" target="_blank">
                <h6>` + titulo + `</h6>
                <small>` + subtitulo + `</small>
                <div class="iconee"><img src="imagens/documentos/icones/icon-` + type + `.png"></div>
                </a>
                <div class="doc_excluir"><img class="doc_excluir_img" src="imagens/documentos/excluir.png"></div>
                <div class="hsdoc" id="excluir_doc` + count + `"></div>
            </span>

            <div id="excluir_doc${count}" class="descdoc descdoccc${count}">
                Deseja realmente excluir este documento?
                <div class='row d-flex noo-margin'>
                    <div class='d-flex col-6 jcc'>
                        <button class='btns canceldoc btndinc ccanceldoc${count}' id='${count}'></button>
                    </div>
                    <div class='d-flex col-6 jcc'>
                        <button class='btns btndcor confirmdoc doc_excluir_btn cconfirmdoc${count}' id='${count}'></button>
                    </div>
                </div>
                
                
            </div>
        
        </div>
        `;

        if(x<12){
            $(`#slide-doc_` + id_slide_doc).append(doc); 
            x++
        }
        else{
            x = 0;
            id_slide_doc++;            
        }

    }

    var id_d = data.length;
    var quant_doc = `
       <h1 class='manipdoc'>` + id_d + `</h1>
       `;
   
       $ ("#total-d").append(quant_doc);
       $('.addbadger').prepend(`<span class="mark-r markd badge badge-secondary">${id_d}</span>`)


    //////EXCLUIR
    $(".doc_excluir_btn").click(function(event) { 
        let num = event.target.id
        key = $('#key_doc' + num);  
        $.ajax({  
            url: 'https://api-dashboard-front.herokuapp.com/documents/' + key[0].value,  
            type: 'DELETE',  
            dataType: 'json',  
            data:key,  
            success: function () {
                
                idhide = '#'+event.target.id
                
                idhide = idhide.replace('excluir', 'item')
                toastr.success('Projeto adicionado!')
                
                $(''+idhide+'').hide()
                var numdoc = $('.manipdoc')[0].innerText
                numdocn = parseInt(numdoc)
                $('.markd')[0].innerText = numdocn-1
                $('.manipdoc')[0].innerText = numdocn-1
                document.getElementById("doc_excluido").click(); 
            },  
            error: function () {  
                toastr.error('O documento não foi excluido, verifique sua conexão com a rede e tente novamente', 'Erro')
            }  
        });

    }); 

/////Search bar documentos
    var lista_titulosd = [];

    for (j = 0; j < data.length; j++) {
        let titulo = data[j].title;
        lista_titulosd.push(titulo);
    }
  autocomplete(document.getElementById("pesquisa-direita"), lista_titulosd);
  
  $("#search-right").click(function(){
    let lista_titulosd2 = [];
    let valConvert2 = [];
    let op = $("#pesquisa-direita");
    let opp = $(op);
    let valor_da_pesquisa = opp[0].value;
    let palavra = valor_da_pesquisa;
    let idx = lista_titulosd.indexOf(palavra);
    let quantidade_telas = $(".sliders_documentos .slide_doc").length;
    let inicial = 1;
  
    for(conv2 = 0; conv2 < lista_titulosd.length; conv2++){
      vallor2 = lista_titulosd[conv2]
      valConvert2.push(format(vallor2))
    }
    palavraConv2 = format(palavra)
    idx = valConvert2.indexOf(palavraConv2)
  
    $(".card-animation").removeClass("card-animation");
    for(i = 0; quantidade_telas > inicial; i++){
      document.getElementById("arrowd_l").click();
      inicial++;
      
    }
    
    while (idx != -1) {
      lista_titulosd2.push(idx);
      idx = lista_titulosd.indexOf(palavra, idx + 1);
    }
    
    localizaDocumento();
  
    function localizaDocumento(){ 
      let id_item_doc = "#item_doc" + lista_titulosd2[0];
      var tamanho_listad = lista_titulosd2[0];
       
  
      if(lista_titulosd2 == "" || lista_titulosd2 ==  null){
        toastr.warning('Tente buscar manualmente', 'Projeto não encontrado')
      }
      for(i = 0;tamanho_listad >= 12; i++){
        document.getElementById("arrowd_r").click();
        tamanho_listad = tamanho_listad - 12;
      }
      $(id_item_doc).addClass("card-animation");
  
      setTimeout(function(){
        $(id_item_doc).removeClass("card-animation");
     }, 1000);
  
    }
    op[0].value = ''
    })


    //deseja realmente excluir este documento?
    
    

    $('.hsdoc').click(function(event){
        conts = event.target.id.substring(11)
        testgh = $('.hsdocpp'+conts+'')[0]
        $(testgh).hide()
        testds = $('.descdoccc'+conts+'')[0]
        $(testds).css('display', 'unset')
    })
      
      $('.canceldoc').click(function(event){
        conts = event.target.id
        testds = $('.descdoccc'+conts+'')[0]
        $(testds).css('display', 'none')
      
        testgh = $('.hsdocpp'+conts+'')[0]
        $(testgh).show()
      })

      $('.doc_excluir_btn').click(function(event){
          conts = event.target.id
          testhgf = $('.doccc'+conts+'')[0]
          $(testhgf).hide()
      })

      ///slider

var margem = 0;
var zero = 0;
$("#arrowd_r").click(function(){
    
    $('.adocl').show()
    $(".sliders_projetos").css("margin-left",`${zero}`);
    var quantidade_sliders = $(".sliders_documentos .slide_doc").length;
    var total_sliders = quantidade_sliders - 1;
    var add_margem = -100;
    var limite = (total_sliders * add_margem);    
    
    
    
    
    if(margem != limite){
        if(margem == limite+100){
            har()
        }     
        margem = margem + add_margem; 
    }
    else{
        har()
    }
    $(".sliders_documentos").css("margin-left", `${margem}vw`);
});
function har(){
    
    $('.adocr').hide()
    $('.adocl').show()
}
function hal(){
    
    $('.adocl').hide()
    $('.adocr').show()
}
function haad(){
    $('.adocl').hide()
    $('.adocr').hide()
}

if(margem == 0){
    $('.adocl').hide()
}

if(($(".sliders_documentos .slide_doc").length-1)*(-100) != 0){
    
    // $('.adocr').hide()
}

$("#arrowd_l").click(function(){
    
    $('.adocr').show()
    var add_margem2 = 100;
    if(margem != 0){
        
        if(margem == -100){
            hal()
        } 
        margem = margem + add_margem2;       
    }
    else{
        hal()
    }
    $(".sliders_documentos").css("margin-left", `${margem}vw`);
});

    

//Reconhecimento de SWIPE
function swipedetect(el, callback){
    var touchsurface = el,
    swipedir,
    startX,
    startY,
    distX,
    distY,
    threshold = 150,
    restraint = 100,
    allowedTime = 300,
    elapsedTime,
    startTime,
    handleswipe = callback || function(swipedir){}
  
    touchsurface.addEventListener('touchstart', function(e){
        var touchobj = e.changedTouches[0]
        swipedir = 'none'
        dist = 0
        startX = touchobj.pageX
        startY = touchobj.pageY
        startTime = new Date().getTime()
    }, false)
  
    touchsurface.addEventListener('touchmove', function(e){
    }, false)
  
    touchsurface.addEventListener('touchend', function(e){
        var touchobj = e.changedTouches[0]
        distX = touchobj.pageX - startX
        distY = touchobj.pageY - startY
        elapsedTime = new Date().getTime() - startTime
        if (elapsedTime <= allowedTime){
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ 
                swipedir = (distX < 0)? 'left' : 'right'
            }
            else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){
                swipedir = (distY < 0)? 'up' : 'down'
            }
        }
        handleswipe(swipedir)
    }, false)
  }
  var el = document.querySelector('.tela3');
  swipedetect(el, function(swipedir){
    if(swipedir == 'right'){
        $("#arrowd_l").click()
    }
    else if(swipedir == 'left'){
        $("#arrowd_r").click()
    }
    
  });

      function format(aaa){
        t1 = aaa.toUpperCase().replace(/\s/g, '')
        assentos = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜ'
      
        for(d1 = 0; d1 < assentos.length ; d1++){
          var alter = 'O'
          var local = t1.indexOf(assentos[d1])
      
          if(local != -1){
            var local2 = assentos.indexOf(t1[local])
      
            if(assentos.substring(0,6).indexOf(assentos[local2]) != -1) {
              alter = 'A'
              t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
            }
            else if(assentos[7].indexOf(assentos[local2]) != -1) {
              alter = 'C'
              t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
            }
            else if(assentos.substring(8,11).indexOf(assentos[local2]) != -1) {
              alter = 'E'
              t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
            }
            else if(assentos.substring(12,15).indexOf(assentos[local2]) != -1) {
              alter = 'I'
              t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
            }
            else if(assentos[16].indexOf(assentos[local2]) != -1) {
              alter = 'D'
              t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
            }
            else if(assentos[17].indexOf(assentos[local2]) != -1) {
              alter = 'N'
              t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
            }
            else if(assentos.substring(18,23).indexOf(assentos[local2]) != -1) {
              alter = 'O'
              t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
            }
            else if(assentos.substring(24,27).indexOf(assentos[local2]) != -1) {
              alter = 'U'
              t1 = (t1.substring(0, local)+alter+t1.substring(local+1))
            }
          }
        }
        return t1
      
      }
      
      function autocomplete(inp, arr){
      
          var currentFocus;
      
          inp.addEventListener("input", function(e){
              var a, b, i, val = this.value;
      
              closeAllLists();
      
              if (!val){
                  return false;
              };
              currentFocus = -1;
      
              a = document.createElement("DIV");
              a.setAttribute("id", this.id + "autocomplete-list");
              a.setAttribute("class", "autocomplete-items");
              
              this.parentNode.appendChild(a);
              
              for (i = 0; i < arr.length; i++) {
                 if (format(arr[i]).replace(' ','').indexOf(format(val).replace(' ','')) != -1) {
                    b = document.createElement("DIV");
                    b.className = "search-force"
                    b.innerHTML = arr[i].substr(0, val.length);
                    b.innerHTML += arr[i].substring(val.length);
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    b.addEventListener("click", function(e) {
                        inp.value = this.getElementsByTagName("input")[0].value;
                        if($('.direita')[0].className != 'direita cheio'){
                          $('#search-left').click()
                        }
                        else{
                          $('#search-right').click()
                        }
                        inp.value = ''
                      
                        closeAllLists();
                    });
                    a.appendChild(b);
                  }
                }
            });
      
            inp.addEventListener("keydown", function(e) {
                if(e.key == "Enter"){
                  if($('.autocomplete-active').length > 0){
                    inp.value = $('.autocomplete-active')[0].children[0].value
                  }
                    
                  if(this.id == 'pesquisa-esquerda'){
                    $('#search-left').click()
                    inp.value = ''
                  }
                  else{
                    $('#search-right').click()
                    inp.value = ''
                  }
                }
      
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                  currentFocus++;
                  addActive(x);
                } else if (e.keyCode == 38) { 
                  currentFocus--;
                  addActive(x);
                } else if (e.keyCode == 13) {
                  e.preventDefault();
                  if (currentFocus > -1) {
                    if (x) x[currentFocus].click();
                  }
                }
            });
            function addActive(x) {
              if (!x) return false;
              removeActive(x);
              if (currentFocus >= x.length) currentFocus = 0;
              if (currentFocus < 0) currentFocus = (x.length - 1);
              x[currentFocus].classList.add("autocomplete-active");
            }
            function removeActive(x) {
              for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
              }
            }
            function closeAllLists(elmnt) {
      
              var x = document.getElementsByClassName("autocomplete-items");
              for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                  x[i].parentNode.removeChild(x[i]);
                }
              }
            }
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
          }
      

})
.fail (function() { 
    
})
.always (function() { 
    
});


}
$("#enviar").click(function(){
    setTimeout(() => { oopa() }, 2000);
    
});