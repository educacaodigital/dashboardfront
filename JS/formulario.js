
$(document).ready(function () {
  $("#submit").click(function (event) {
      event.preventDefault();


      var form = $('#form-insercao')[0];
      var data = new FormData(form);
      
      $.ajax({
          type: "POST",
          enctype: 'multipart/form-data',
          url: "https://api-dashboard-front.herokuapp.com/projects",
          data: data,
          processData: false, 
          contentType: false, 
          cache: false, 
          timeout: 600000, 
          success: function (data) {
            toastr.success('Projeto Adicionado')
              // $(".botao-form p").removeClass("hide");
              // $(".loader").removeClass("show");                  
          },
          error: function (e) {
            toastr.success('Verifique sua conexão com a internet','Projeto não adicionado')
            // $(".botao-form p").removeClass("hide");
            // $(".loader").removeClass("show");
          }
      });
  });



  


});

  $("#ins_button").click(function(){
    location.reload();
  });
///////////////////////////////
$("#doc_button").click(function(){
  location.reload();
});

$(document).ready(function () {
  $("#enviar").click(function (event) {
      event.preventDefault();

      $(".botao-form p").addClass("hide");
      $(".loader").addClass("show");
      
      var form = $('#form-documentos')[0];
      $("#enviar").prop("disabled", true);
      
      let opa = {
        title: recebeValorInput($('#doc-titulo')),
        subtitle: recebeValorInput($('#doc-subtitulo')),
        url: recebeValorInput($('#doc-url')),
        type: recebeValorInput($('#doc-type'))
    }
    let opaBolado = JSON.stringify(opa);

      $.ajax({
          type: "POST",
          enctype: 'application/json',
          url: "https://api-dashboard-front.herokuapp.com/documents",
          data: opaBolado,
          processData: false, 
          contentType: 'application/json',
          cache: false,  
          timeout: 600000, 
          success: function (data) {
              $("#enviar").prop("disabled", false);
              $(".botao-form p").removeClass("hide");
              $(".loader").removeClass("show");
          },
          error: function (e) {  
            $("#enviar").prop("disabled", false);
            $(".botao-form p").removeClass("hide");
            $(".loader").removeClass("show");

          }
      });
      
  });
});

function recebeValorInput(campo) {
  let valor 
  valor = campo.val();

  return valor
}



